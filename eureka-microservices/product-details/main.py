import py_eureka_client.eureka_client as eureka_client
from flask import Flask, request, jsonify
from openai import OpenAI
from flask_cors import CORS

rest_port = 8050
eureka_client.init(eureka_server="http://localhost:8761/eureka",
                   app_name="product-details",
                   instance_port=rest_port)

app = Flask(__name__)
CORS(app, origins='*')
client = OpenAI(api_key="api_key")

@app.route('/product-details', methods=['POST'])
def get_product_details():
    data = request.json
    if 'product' not in data:
        return jsonify({'error': 'Product not provided'})

    product = data['product']
    stream = client.chat.completions.create(
        model="gpt-3.5-turbo-1106",
        messages=[{"role": "user", "content": f"Give me product details: {product}"}],
        stream=True,
    )
    response = ''
    for chunk in stream:
        if chunk.choices[0].delta.content is not None:
            response += chunk.choices[0].delta.content

    return jsonify({'product_details': response})

if __name__ == '__main__':
    app.run(host='0.0.0.0',port = rest_port)