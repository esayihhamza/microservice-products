# Computer Components Microservices Application

This application is a microservices-based system designed to manage computer components' data and provide detailed product descriptions using OpenAI's GPT-3.5 model.

## Components

### Eureka Server
- Responsible for service registration and discovery.
- Technologies: Spring Boot

### Product Service
- Manages CRUD operations for computer components (products).
- Technologies: Spring Boot, MySQL, RESTful APIs

### Product Details
- Python application using OpenAI's GPT-3.5 model.
- Generates detailed product descriptions based on product names.

### Product Frontend
- React-based frontend application.
- Consumes services from Product Service and Product Details.
- Provides a user interface for viewing, adding, updating, and deleting computer components.

## Setup Instructions

### Eureka Server
- Navigate to the `eureka-server` directory.
- Run the Spring Boot application.
- Access the Eureka dashboard: http://localhost:8761

### Product Service
- Navigate to the `product-service` directory.
- Configure the database settings in `application.properties`.
- Run the Spring Boot application.
- Access CRUD operations at: http://localhost:8060/product-service

### Product Details
- Set up Python environment with required dependencies.
- Start the Python application.
- Ensure it's running and accessible.
- Access details service at: http://localhost:8050/

### Product Frontend
- Navigate to the `product-frontend` directory.
- Install dependencies: `npm install`.
- Start the React application: `npm start`.
- Access the frontend interface: http://localhost:3000